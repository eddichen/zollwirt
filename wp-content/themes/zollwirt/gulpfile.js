var gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
        pattern: ['gulp-*']
    }),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    fs = require('fs');

/** serve **/

gulp.task('serve', function() {

    browserSync.init({
        proxy: "zollwirt.local"
    });

    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('js/**/*.js', ['scripts']);
});

/** sass **/

gulp.task('sass', function() {
    return gulp.src("sass/**/*.scss", {base: 'sass'})
        .pipe($.sourcemaps.init())
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.sass({
            outputStyle: "compressed",
            sourceMap: true
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest("./"))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

/** javascript **/

gulp.task('scripts', function () {
    return gulp.src([
        './js/lib/*.js',
        './js/zollwirt/*.js'
    ])
        .pipe($.concat('scripts.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('./js/'));
});

gulp.task('build', ['scripts','sass']);