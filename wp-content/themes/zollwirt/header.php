<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>

    <header id="masthead" class="site-header" role="banner">
        <div class="site-branding">
            <div class="site-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-logo__link"><img src="<?php bloginfo('template_directory'); ?>/images/logo-zollwirt.png" class="site-logo__img" /></a>
            </div>
            <?php
                if ( is_front_page() && is_home() ) : ?>
                    <h1 class="site-title visuallyhidden"><?php bloginfo( 'name' ); ?></h1>
                <?php else : ?>
                    <p class="site-title visuallyhidden"><?php bloginfo( 'name' ); ?></p>
                <?php endif;

                $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) : ?>
                    <p class="site-description"><?php echo $description; ?></p>
                <?php endif;
            ?>
            <button class="secondary-toggle"><?php _e( 'Menu and widgets', 'twentyfifteen' ); ?></button>
            <!--<div class="site-switch">
                <div class="site-switch__current">
                    <div class="site-switch__current-link flag-icon flag-icon-de"><span>Deutsch</span></div>
                </div>
                <div class="site-switch__options">
                    <ul class="site-switch__list">
                        <li class="site-switch__item"><p class="site-switch__item-label">Change Language</p></li>
                        <li class="site-switch__item site-switch__de"><a href="<?php echo network_site_url(); ?>" class="site-switch__link flag-icon flag-icon-de"><span>Deutsch</span></a></li>
                        <li class="site-switch__item site-switch__it"><a href="<?php echo network_site_url(); ?>it" class="site-switch__link flag-icon flag-icon-it"><span>Italiano</span></a></li>
                        <li class="site-switch__item site-switch__en"><a href="<?php echo network_site_url(); ?>en" class="site-switch__link flag-icon flag-icon-gb"><span>English</span></a></li>
                    </ul>
                </div>
            </div>-->
        </div><!-- .site-branding -->
    </header><!-- .site-header -->

    <?php get_sidebar(); ?>

	<div id="content" class="site-content">
