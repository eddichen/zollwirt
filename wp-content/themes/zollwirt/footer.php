<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer footer" role="contentinfo">
        <div class="footer__content">
            <div class="footer__details" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <p class="footer__details-item"><span itemprop="streetAddress">Oberotte 60</span><br /> <span itemprop="postalCode">9963</span> <span itemprop="addressLocality">St. Jakob im Defereggental,<br /> Austria</span></p>
                <p class="footer__details-item">T: <a href="tel:+4348735225" itemprop="telephone">+43 4873 5225</a></p>
                <p class="footer__details-item">F: <span itemprop="faxNumber">+43 4873 522525</span></p>
                <p class="footer__details-item">E: <a href="mailto:info@zollwirt.at" itemprop="email">info@zollwirt.at</a></p>
            </div>
            <ul class="footer__social">
                <li class="footer__social-item"><a href="https://www.facebook.com/pages/Gasthof-Zollwirt/100594916663674" class="footer__social-link"><i class="icon icon-facebook-squared"><span class="icon__text">Facebook</span></i></a></li>
                <li class="footer__social-item"><a href="https://twitter.com/Zollwirtspeter" class="footer__social-link"><i class="icon icon-twitter-squared"><span class="icon__text">Twitter</span></i></a></li>
                <li class="footer__social-item"><a href="https://www.instagram.com/zollwirt/" class="footer__social-link"><i class="icon icon-instagram"><span class="icon__text">Instagram</span></i></a></li>
                <li class="footer__social-item"><a href="https://plus.google.com/112278075145634790384/about" class="footer__social-link"><i class="icon icon-gplus-squared"><span class="icon__text">Google Plus</span></i></a></li>
            </ul>
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup"  class="subscribe">
                <form action="//zollwirt.us1.list-manage.com/subscribe/post?u=a6ccd5e0248c55654df08a56d&amp;id=c712244d23" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <label for="mce-EMAIL" class="subscribe__label">Subscribe to our mailing list</label>
                        <input type="email" value="" name="EMAIL" class="email subscribe__input" id="mce-EMAIL" placeholder="email address" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a6ccd5e0248c55654df08a56d_c712244d23" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button subscribe__button"></div>
                    </div>
                </form>
            </div>
            <!--End mc_embed_signup-->
            <ul class="footer__links">
                <li class="footer__links-item"><a href="#">Location</a></li>
                <li class="footer__links-item"><a href="#">Contact</a></li>
                <li class="footer__links-item"><a href="#">Sitemap</a></li>
            </ul>
        </div>
	</footer><!-- .site-footer -->

</div><!-- .site -->
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/scripts.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>
