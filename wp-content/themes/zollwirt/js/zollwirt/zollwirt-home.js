var Zollwirt = Zollwirt || {};
//undefined is passed through to make sure undefined is actually undefined as a lot of libraries overwrite this
Zollwirt.Homepage = (function ($, undefined) {
    "use strict";
    var ZollwirtHomepage = {};

    ZollwirtHomepage.tripadvisor = function() {
        /* removing the width from the tripadvisor widget */
        $(window).load(function(){
            $(".widSSP").attr("style","");
        });
    };

    ZollwirtHomepage.init = function() {
        ZollwirtHomepage.tripadvisor();
    };

    return ZollwirtHomepage;

})(window.jQuery);