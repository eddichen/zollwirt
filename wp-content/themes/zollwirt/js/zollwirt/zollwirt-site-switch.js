var Zollwirt = Zollwirt || {};
//undefined is passed through to make sure undefined is actually undefined as a lot of libraries overwrite this
Zollwirt.SiteSwitch = (function ($, undefined) {
    "use strict";
    var ZollwirtSiteSwitch = {};

    ZollwirtSiteSwitch.menuPosition = function() {
        var mQuery = Modernizr.mq('(max-width: 955px)');
        if(mQuery) {
            $('.menu-main-nav-container').append($('.site-switch'));
        }
        else {
            $('.site-branding').append($('.site-switch'));
        }
    };

    ZollwirtSiteSwitch.languageLogo = function() {
        var $currentLogo = $('.site-switch__current-link');
        var pathArray = window.location.pathname.split( '/' );
        var store = pathArray[1];
        switch(store){
            case "en":
                $currentLogo.attr("class", "site-switch__current-link flag-icon flag-icon-gb").find("span").text("English");
                $('.site-switch__en').hide();
                break;
            case "it":
                $currentLogo.attr("class", "site-switch__current-link flag-icon flag-icon-it").find("span").text("Italiano");
                $('.site-switch__it').hide();
                break;
            default:
                $currentLogo.attr("class", "site-switch__current-link flag-icon flag-icon-de").find("span").text("Deutsch");
                $('.site-switch__de').hide();
                break;
        }
    };

    ZollwirtSiteSwitch.init = function() {
        ZollwirtSiteSwitch.menuPosition();
        ZollwirtSiteSwitch.languageLogo();
        /* TODO fix smartresize
        $(window).on('smartresize', function(){
            ZollwirtSiteSwitch.menuPosition();
        });*/
        $(window).on('resize', function() {
            ZollwirtSiteSwitch.menuPosition();
        });
    };

    return ZollwirtSiteSwitch;

})(window.jQuery);